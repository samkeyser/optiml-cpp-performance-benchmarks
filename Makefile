# https://makefiletutorial.com/

SHELL = /bin/bash

INCLUDE = -I.

CC ?= ~/compilers/base-clang/bin/clang
CXX ?= ~/compilers/base-clang/bin/clang++
LLC ?= ~/compilers/base-clang/bin/llc

CFLAGS = $(INCLUDE) -O3
CPPFLAGS = -std=c++14 -lstdc++ $(INCLUDE) -O3

EXTRA_CFLAGS ?=
EXTRA_CXXFLAGS ?=
CFLAGS += $(EXTRA_CFLAGS)
CXXFLAGS += $(EXTRA_CXXFLAGS)

CLIBS = -lm
CPPLIBS = -lm

DEPENDENCYFLAG = -M

BINARIES=machine \
functionobjects \
loop_removal \
locales \
rotate_bits \
scalar_replacement_structs


#% : %.cpp
#	@echo "Generating LLVM IR"
#	$(CXX) -S -emit-llvm $(CPPFLAGS) $(CXXFLAGS) $^ -o $(addsuffix .ll, $(basename $@))
#	
#	@echo "Generating LLVM ASM"
#	$(LLC) $(addsuffix .ll, $(basename $^))
#	
#	@echo "Generating LLVM object file"
#	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $^ -o $(addsuffix .o, $(basename $^))
#	
#	@echo "Generating executable file"
#	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(addsuffix .s, $(basename $^)) -o $@

all : $(BINARIES)

REPORT_FILE = report.txt
report:  $(BINARIES)
	echo "##STARTING Version 1.0" > $(REPORT_FILE)
	date >> $(REPORT_FILE)
	echo "##CFlags: $(CFLAGS)" >> $(REPORT_FILE)
	echo "##CXXFlags: $(CXXFLAGS)" >> $(REPORT_FILE)
	
	export LLVM_PROFILE_FILE="file-%m.profraw"
	numactl --physcpubind=6 perf record -g -o machine.perf ./machine >> $(REPORT_FILE)
	sudo /sbin/sysctl vm.drop_caches=1
	
	numactl --physcpubind=6 perf record -g -o functionobjects.perf ./functionobjects >> $(REPORT_FILE)
	sudo /sbin/sysctl vm.drop_caches=1
	
	numactl --physcpubind=6 perf record -g -o loop_removal.perf ./loop_removal >> $(REPORT_FILE)
	sudo /sbin/sysctl vm.drop_caches=1
	
	numactl --physcpubind=6 perf record -g -o locales.perf ./locales >> $(REPORT_FILE)
	sudo /sbin/sysctl vm.drop_caches=1
	
	numactl --physcpubind=6 perf record -g -o rotate_bits.perf ./rotate_bits >> $(REPORT_FILE)
	sudo /sbin/sysctl vm.drop_caches=1
	
	numactl --physcpubind=6 perf record -g -o scalar_replacement_structs.perf ./scalar_replacement_structs >> $(REPORT_FILE)
	sudo /sbin/sysctl vm.drop_caches=1
	
	date >> $(REPORT_FILE)
	echo "##END Version 1.0" >> $(REPORT_FILE)

package : $(BINARIES)
	mkdir -p $(DESTDIR)
	mv -t $(DESTDIR) $(addsuffix .ll, $(BINARIES))
	mv -t $(DESTDIR) *.profraw
	mv -t $(DESTDIR) $(addsuffix .perf, $(BINARIES))

# declare some targets to be fakes without real dependencies
.PHONY : clean all

clean :
		rm -f *.o *.s *.ii *.bc *.ll *.perf $(BINARIES) report.txt
