#!/usr/bin/env bash

# make the executables
#CC=$HOME/compilers/base-clang/bin/clang CXX=$HOME/compilers/base-clang/bin/clang++ \
#    EXTRA_CFLAGS="-fprofile-generate -fno-omit-frame-pointer" \
#    EXTRA_CXXFLAGS="-fprofile-generate -fno-omit-frame-pointer" \
#    make all

export CC=$HOME/compilers/base-clang/bin/clang CXX=$HOME/compilers/base-clang/bin/clang++
export EXTRA_CFLAGS="-fprofile-generate -fno-omit-frame-pointer -fembed-bitcode=all -save-temps=obj"
export EXTRA_CXXFLAGS="-fprofile-generate -fno-omit-frame-pointer -fembed-bitcode=all -save-temps=obj"
bear -- make all
